import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { HerosComponent } from "./heros.component";
import { HerostestComponent } from "./herostest/herostest.component";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { HeroService } from "./hero.service";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { HttpClientModule } from "@angular/common/http";
import { InMemoryWebApiModule } from "angular-in-memory-web-api";
import { InMemDataService } from "./in-memory-data.service";
import { MessagesComponent } from "./messages/messages.component";
import { MessageService } from "./message.service";

@NgModule({
  declarations: [
    AppComponent,
    HerosComponent,
    HerostestComponent,
    DashboardComponent,
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    InMemoryWebApiModule.forRoot(InMemDataService)
  ],
  providers: [HeroService, MessageService],
  bootstrap: [AppComponent],
  exports: [HerostestComponent]
})
export class AppModule {}
