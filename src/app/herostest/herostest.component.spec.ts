import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HerostestComponent } from './herostest.component';

describe('HerostestComponent', () => {
  let component: HerostestComponent;
  let fixture: ComponentFixture<HerostestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HerostestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HerostestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
