import { NgModule, Component } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HerosComponent } from "./heros.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { HerostestComponent } from "./herostest/herostest.component";

const routes: Routes = [
  {
    path: "heros",
    component: HerosComponent
  },
  {
    path: "dashboard",
    component: DashboardComponent
  },
  {
    path: "",
    redirectTo: "/dashboard",
    pathMatch: "full"
  },
  {
    path: "detail/:id",
    component: HerostestComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
