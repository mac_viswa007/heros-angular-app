import { Hero } from "./hero";
export const HEROS: Hero[] = [
  { id: 1, name: "Tony stark" },
  { id: 2, name: "Mark abron" },
  { id: 3, name: "Tornado" },
  { id: 4, name: "Dr IQ" },
  { id: 5, name: "Magneta" },
  { id: 6, name: "Magma" },
  { id: 7, name: "RubberMan" }
];
