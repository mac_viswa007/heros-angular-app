import { Component, OnInit, Input } from "@angular/core";
import { Hero } from "../hero";
import { ActivatedRoute, Params } from "@angular/router";
import { HeroService } from "../hero.service";
import { switchMap } from "rxjs/operators";
import { Location } from "@angular/common";

@Component({
  selector: "hero-detail",
  templateUrl: "./herostest.component.html",
  styleUrls: ["./herostest.component.scss"]
})
export class HerostestComponent implements OnInit {
  @Input() hero: Hero;
  constructor(
    private heroservice: HeroService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.route.params
      .pipe(
        switchMap((params: Params) => this.heroservice.getHero(+params["id"]))
      )
      .subscribe(hero => (this.hero = hero));
  }

  save() {
    this.heroservice.updateHero(this.hero).subscribe(() => this.goBack());
  }
  goBack(): void {
    this.location.back();
  }
}
