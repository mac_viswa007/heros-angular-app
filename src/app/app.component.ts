import { Component } from "@angular/core";
@Component({
  selector: "app-root",
  template: `
    <h1>{{ title }}</h1>
    <nav>
      <a routerLink="/dashboard" routerLinkActive="active">Dashboard</a>
      <a routerLink="/heros" routerLinkActive="active">Heroes</a>
    </nav>
    <app-messages></app-messages>
    <router-outlet></router-outlet>
  `,
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "Tour of Heroes";
}
