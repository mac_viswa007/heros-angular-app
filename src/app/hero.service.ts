import { Injectable } from "@angular/core";
import { Hero } from "./hero";
import { HEROS } from "./mock-heroes";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MessageService } from "./message.service";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
@Injectable()
export class HeroService {
  private herosURL = "api/heroes";
  httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
  };
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}
  getHeroes(): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.herosURL).pipe(
      tap(_ => this.log("fetch heroes")),
      catchError(this.handleError<Hero[]>("getHeros", []))
    );
  }

  getHero(id: number): Observable<Hero> {
    const url = `${this.herosURL}/${id}`;
    return this.http.get<Hero>(url).pipe(
      tap(_ => this.log(`fetch hero by id : ${id}`)),
      catchError(this.handleError<Hero>(`getHero by id: ${id}`))
    );
  }

  private log(message: string) {
    this.messageService.add(`Heroservice: ${message}`);
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      this.log(`${operation}failed : ${error.message}`);
      return of(result as T);
    };
  }

  updateHero(hero: Hero): Observable<any> {
    return this.http.put(this.herosURL, hero, this.httpOptions).pipe(
      tap(_ => this.log(`Update hero id = ${hero.id}`)),
      catchError(this.handleError<Hero>(`udpate hero: ${hero.id}`))
    );
  }
}
