import { InMemoryDbService } from "angular-in-memory-web-api";
import { Hero } from "./hero";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class InMemDataService implements InMemoryDbService {
  createDb() {
    const heroes: Hero[] = [
      { id: 11, name: "Tony stark" },
      { id: 12, name: "Mark abron" },
      { id: 13, name: "Tornado" },
      { id: 14, name: "Dr IQ" },
      { id: 15, name: "Magneta" },
      { id: 16, name: "Magma" },
      { id: 17, name: "RubberMan" }
    ];

    return { heroes };
  }

  genId(heros: Hero[]): number {
    return heros.length > 0 ? Math.max(...heros.map(hero => hero.id)) + 1 : 11;
  }
}
