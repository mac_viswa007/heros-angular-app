import { Component, OnInit } from "@angular/core";
import { Hero } from "./hero";
import { HeroService } from "./hero.service";
import { Router } from "@angular/router";

@Component({
  selector: "my-heros",
  templateUrl: "./heros.component.html",
  styleUrls: ["./heros.component.scss"],
  providers: [HeroService]
})
export class HerosComponent implements OnInit {
  heros: Hero[];
  selectedHero: Hero;

  constructor(private heroService: HeroService, private router: Router) {}

  ngOnInit() {
    this.getHeros();
  }

  getHeros(): void {
    this.heroService.getHeroes().subscribe(heros => (this.heros = heros));
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  goToDetail(): void {
    this.router.navigate(["/detail", this.selectedHero.id]);
  }
}
